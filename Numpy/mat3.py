import numpy as np

matrice = np.array(([1,3,3],[1,4,3],[1,3,4]))

print("matrice", matrice)
print("----------------------")
determinant = np.linalg.det(matrice)
print("determinant", determinant)

print("-----------------------")
matriceInv = np.linalg.inv(matrice)
print("inverse", matriceInv)

print("-----------------------")
a = np.array([[2,3,3,1], [-4,-6,3,2], [-1,1,1,1], [-2,-1,1,1]])
b = np.array([15,3,5,1])
print("A:", a)
print("B:", b)
x = np.linalg.solve(a,b)
print("Solve for x=", x)
print("-----------------------")
matriceStochRegu = np.array([[0.5,0.3,0.2],[0.2,0.8,0.0],[0.3,0.3,0.4]])
i = 0
v, P = np.linalg.eig(matriceStochRegu)
print(np.allclose(matriceStochRegu.dot(P[:, i]), v[i] * P[:, i]))
imax = np.argmax(np.abs(v))
x = P[:, imax]  # le vecteur propre recherché
print("Valeur propre : " + v[imax]) #valeur propre liée


