import numpy as np

matrice1 = np.random.randint(0, 8+1, size=(3, 3))
matrice2 = np.random.randint(2, 10+1, size=(3, 3))

print("mat1", matrice1)
print("----------------------")
print("mat2", matrice2)

print("----------- mat1 dot mat2 ------------")
matriceDot = matrice1.dot(matrice2)
print(matriceDot)

print("----------- mat1 * mat2 ------------")
matriceStar = matrice1 * matrice2
print(matriceStar)

print("----------- mat1 transpo------------")
matrice1Tran = matrice1.transpose()
print(matrice1Tran)