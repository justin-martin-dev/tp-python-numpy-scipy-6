import numpy as np
import matplotlib.pyplot as plt
from scipy.optimize import curve_fit

def logistique(x, K, a, r):
    """ Fonction logistique (Verhulst)
    Paramètres K, a, r.
    Voir : https://fr.wikipedia.org/wiki/Fonction_logistique_(Verhulst)
    """
    y = K / (1.0 + a * np.exp(r * -x))
    return y


x = np.linspace(-10, 15, 100)
y = logistique(x, 150, 10, 0.65)

plt.plot(x, y, color="yellow", label="y")

""" Ajouter du bruit (Gaussian distribution) 
pour simuler les données expérimentales.
"""
noise = 1.10 * np.random.normal(size=y.size)
y_noise = y + noise

plt.scatter(x, y_noise, s=5, c="red", label="y_noise")

""" Curve Fit """
init_vals = [10, 1, 0.1]  # for [K, a, r]
best_vals, covar = curve_fit(logistique, x, y_noise, p0=init_vals)
print('best_vals: {}'.format(best_vals))

y_fit = logistique(x, best_vals[0], best_vals[1], best_vals[2])
plt.plot(x, y_fit, label="y_fit")

plt.legend()
plt.grid()
#plt.show()